
open Lang.NuL.Ast

module Env = Common.Var.Set

let rec check_expr env expr =
  match expr.e_kind with
  | Succ  -> true
  | Var x -> Env.mem x env
  | Num _ -> true
  | Abs(x, body) -> check_expr (Env.add x env) body
  | App(e1, e2)  -> check_expr env e1 && check_expr env e2
  | Case(expr, case_fun, case_zero, case_succ) ->
    check_expr env expr
    && check_case_fun  env case_fun
    && check_case_zero env case_zero
    && check_case_succ env case_succ

and check_case_fun env (x, body) =
  check_expr (Env.add x env) body

and check_case_zero env body =
  check_expr env body

and check_case_succ env (x, body) =
  check_expr (Env.add x env) body

let check_program expr =
  check_expr Env.empty expr

let register () =
  Compiler.register_contract_checker
    ~lang: Compiler.Lang_NuL
    ~name: "NuL:right_scopes"
    ~contract: Lang.NuL.Contracts.right_scopes
    check_program
