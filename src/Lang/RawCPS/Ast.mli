
type accesspath =
  { ap_tag  : Common.Tag.t
  ; ap_kind : accesspath_kind
  }
and accesspath_kind =
| Offp of int
| Selp of int * accesspath

type value_kind =
| Var    of string
| Label  of string
| Int    of int64
| Real   of Common.Real.t
| String of string

type value =
  { v_tag  : Common.Tag.t
  ; v_kind : value_kind
  }

type expr =
  { e_tag  : Common.Tag.t
  ; e_kind : expr_kind
  }
and expr_kind =
| Record    of (value * accesspath) list * string * expr
| Select    of int * value * string * expr
| Offset    of int * value * string * expr
| App       of value * value list
| Fix       of (string * string list * expr) list * expr
| Switch    of value * expr list
| Primop    of string * value list * string list * expr list
