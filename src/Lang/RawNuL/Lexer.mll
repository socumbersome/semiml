{

exception Invalid_character of char
exception Invalid_number    of string

let kw_map = Hashtbl.create 32
let _ =
  Hashtbl.add kw_map "case" YaccParser.KW_CASE;
  Hashtbl.add kw_map "fun"  YaccParser.KW_FUN;
  Hashtbl.add kw_map "of"   YaccParser.KW_OF;
  Hashtbl.add kw_map "succ" YaccParser.KW_SUCC;
  Hashtbl.add kw_map "zero" YaccParser.KW_ZERO;
  ()

let tokenize_ident str =
  try Hashtbl.find kw_map str with
  | Not_found -> YaccParser.ID str

let nat_regexp = Str.regexp "^[0-9]+$"

let tokenize_number str =
  if Str.string_match nat_regexp str 0 then
    try YaccParser.NAT (int_of_string str) with
    | Failure _ -> raise (Invalid_number str)
  else
    raise (Invalid_number str)

}

let digit    = ['0'-'9']
let lower    = ['a'-'z']
let upper    = ['A'-'Z']
let var_char = lower | upper | digit | '_' | '\''

rule token = parse
    [ ' ' '\r' '\t' ]+ { token lexbuf }
  | '\n' { Lexing.new_line lexbuf; token lexbuf }
  | "//" { line_comment lexbuf }
  | "(*" { block_comment 1 lexbuf }
  | '('  { YaccParser.BR_OPN }
  | ')'  { YaccParser.BR_CLS }
  | "->" { YaccParser.ARROW  }
  | '|'  { YaccParser.BAR    }
  | (lower | upper | '_') var_char* as x { tokenize_ident x }
  | digit (var_char | '.')* as x { tokenize_number x }
  | eof { YaccParser.EOF }
  | _ as x { raise (Invalid_character x) }

and line_comment = parse
    '\n' { Lexing.new_line lexbuf; token lexbuf }
  | eof { YaccParser.EOF }
  | [^ '\n']+ { line_comment lexbuf }

and block_comment depth = parse
    '\n' { Lexing.new_line lexbuf; block_comment depth lexbuf }
  | "(*" { block_comment (depth+1) lexbuf }
  | "*)" {
      if depth = 1 then token lexbuf
      else block_comment (depth-1) lexbuf
    }
  | eof  {
      Errors.error_lp lexbuf.Lexing.lex_curr_p
        "End of file inside block comment. `*)' was expected.";
      raise Errors.Fatal_error
    }
  | _ { block_comment depth lexbuf }
