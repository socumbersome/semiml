
type t

val compare : t -> t -> int
val equal   : t -> t -> bool

val create :
  ?description: string ->
  ?languages:   Language.t list ->
    string -> t

val name        : t -> string
val description : t -> string option
val languages   : t -> Language.t list option

val pretty : t -> Printing.Box.t

val iter : (t -> unit) -> unit

type action =
| Add    of t list
| Set    of t list
| Remove of t list

type rule = t list * action

val saves_contract : t -> rule

module Set : Set.S with type elt = t

val apply_rules : rule list -> Set.t -> Set.t
