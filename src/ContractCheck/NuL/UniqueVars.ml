
open Lang.NuL.Ast

let used_vars = Hashtbl.create 32

let check_var x =
  if Hashtbl.mem used_vars x then false
  else begin
    Hashtbl.add used_vars x ();
    true
  end

let rec check_expr expr =
  match expr.e_kind with
  | Succ | Var _ | Num _ -> true
  | Abs(x, body) -> check_var x && check_expr body
  | App(e1, e2)  -> check_expr e1 && check_expr e2
  | Case(e1, (x2, e2), e3, (x4, e4)) ->
    check_expr e1 && 
    check_var x2 && check_expr e2 && 
    check_expr e3 && 
    check_var x4 && check_expr e4

let check_program expr =
  Hashtbl.reset used_vars;
  check_expr expr

let register () =
  Compiler.register_contract_checker
    ~lang: Compiler.Lang_NuL
    ~name: "NuL:unique_vars"
    ~contract: Lang.NuL.Contracts.unique_vars
    check_program
