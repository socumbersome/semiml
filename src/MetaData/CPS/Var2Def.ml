open Lang.CPS.Ast

let raise_not_found name =
  let err = Printf.sprintf "MetaData::CPS::Var2Def: variable %s not found" (Common.Var.string_of_var name)
  in begin
    CompilerLog.error "%s" err;
    failwith err;
  end

let table = Hashtbl.create 1024

let get vr = 
  try Hashtbl.find table vr with
  | Not_found -> raise_not_found vr

let try_get vr =
  try Some(Hashtbl.find table vr) with
  | Not_found -> None

let set v def =
  Hashtbl.replace table v def
