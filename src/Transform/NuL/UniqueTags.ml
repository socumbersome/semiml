
open Lang.NuL.Ast

let used_tags = Hashtbl.create 32

let unique_tag tag =
  if Hashtbl.mem used_tags tag then begin
    let tag = MetaData.Tags.copy tag in
    Hashtbl.add used_tags tag ();
    tag
  end else begin
    Hashtbl.add used_tags tag ();
    tag
  end

let rec unique expr =
  { e_tag  = unique_tag expr.e_tag
  ; e_kind =
    match expr.e_kind with
    | (Succ | Var _ | Num _) as kind -> kind
    | Abs(x, body) -> Abs(x, unique body)
    | App(e1, e2)  -> App(unique e1, unique e2)
    | Case(e1, (x2, e2), e3, (x4, e4)) ->
      Case(unique e1, (x2, unique e2), unique e3, (x4, unique e4))
  }

let transform expr =
  Hashtbl.reset used_tags;
  unique expr

let c_unique_tags = Contract.create
  ~description: "Restore unique_tags contract"
  ~languages: [Language.NuL]
  "transform:unique_tags"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_NuL
    ~target: Compiler.Lang_NuL
    ~name:   "NuL:unique_tags"
    ~contracts:
      [ Lang.NuL.Contracts.unique_tags
      ; c_unique_tags
      ]
    ~contract_rules:
      [ Contract.saves_contract Lang.NuL.Contracts.right_scopes
      ; Contract.saves_contract Lang.NuL.Contracts.unique_vars
      ]
    transform
