
open Lang.CPS.Ast

module MD = MetaData.CPS.Var2Def

let rec analyse_expr expr = match expr.e_kind with
  | Record(_, x, e) -> MD.set x expr; analyse_expr e
  | Select(_, _, x, e) -> MD.set x expr; analyse_expr e
  | Offset(_, _, x, e) -> MD.set x expr; analyse_expr e
  | App(v, vs) -> ()
  | Fix(decls, e) -> List.iter (
      fun (vname, formals, body) -> begin
        MD.set vname expr;
        analyse_expr body;
      end
    ) decls
    ; analyse_expr e
  | Switch(_, es) -> List.iter analyse_expr es
  | Primop(_, _, xs, es) -> List.iter (fun x -> MD.set x expr) xs;
    List.iter analyse_expr es

let analyse expr =
  let _ = analyse_expr expr in ()

let contract = Contract.create
  ~description: ("Caches a mapping from each "
              ^ "variable to the expression that "
              ^ "it created (binding site)")
  ~languages: [Language.CPS]
  "analyse:var2def"

let register () =
  Compiler.register_analysis
    ~lang:      Compiler.Lang_CPS
    ~name:      "CPS:var2def"
    ~require:   [ ]
    ~contracts: [ contract ]
    analyse

