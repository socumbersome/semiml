
type primop_type =
| TUnit
| TBool
| TInt
| TReal
| TString
| TExn
| TVar    of int
| TRef    of primop_type
| TCont   of primop_type
| TArray  of primop_type
| TArrow  of primop_type * primop_type

module type Group = sig
  type t

  (** [is_operator op] determines if op is operator or predefined constant
  (useful for pretty-printing *)
  val is_operator : t -> bool

  val name        : t -> string
  val alpha_name  : t -> string
  val arity       : t -> int
  val cps_value_n : t -> int
  val cps_cont_n  : t -> int

  val type_of : t -> int * primop_type list * primop_type

  val all_values : t list
end

module Arith : sig
  type t =
  | Add | Sub
  | Neg
  | Mul | Div | Mod
  | RShift | LShift
  | Orb | Andb | Xorb
  | Notb
  | Lt  | Le  | Gt  | Ge
  | RangeCheck

  val is_operator : t -> bool
  val name        : t -> string
  val alpha_name  : t -> string
  val arity       : t -> int
  val cps_value_n : t -> int
  val cps_cont_n  : t -> int

  val type_of : t -> int * primop_type list * primop_type

  val all_values : t list
end

module FArith : sig
  type t =
  | FAdd | FSub
  | FMul | FDiv
  | FEq  | FNeq
  | FLt  | FLe  | FGt  | FGe

  val is_operator : t -> bool
  val name        : t -> string
  val alpha_name  : t -> string
  val arity       : t -> int
  val cps_value_n : t -> int
  val cps_cont_n  : t -> int

  val type_of : t -> int * primop_type list * primop_type

  val all_values : t list
end

module Mem : sig
  type t =
  | Deref | Subscript | OrdOf
  | Assign | UnboxedAssign
  | Update | UnboxedUpdate | Store
  | MakeRef | MakeRefUnboxed
  | AllocArray | AllocUnboxedArray | AllocByteArray

  val is_operator : t -> bool
  val name        : t -> string
  val alpha_name  : t -> string
  val arity       : t -> int
  val cps_value_n : t -> int
  val cps_cont_n  : t -> int

  val type_of : t -> int * primop_type list * primop_type

  val all_values : t list
end

module Repr : sig
  type t =
  | Boxed
  | ALength | SLength
  | PhysEq | PhysNEq

  val is_operator : t -> bool
  val name        : t -> string
  val alpha_name  : t -> string
  val arity       : t -> int
  val cps_value_n : t -> int
  val cps_cont_n  : t -> int

  val type_of : t -> int * primop_type list * primop_type

  val all_values : t list
end

module Exn : sig
  type t =
  | GetHdlr
  | SetHdlr

  val is_operator : t -> bool
  val name        : t -> string
  val alpha_name  : t -> string
  val arity       : t -> int
  val cps_value_n : t -> int
  val cps_cont_n  : t -> int

  val type_of : t -> int * primop_type list * primop_type

  val all_values : t list
end

module Control : sig
  type t =
  | CallCC
  | Throw

  val is_operator : t -> bool
  val name        : t -> string
  val alpha_name  : t -> string
  val arity       : t -> int
  val cps_value_n : t -> int
  val cps_cont_n  : t -> int

  val type_of : t -> int * primop_type list * primop_type

  val all_values : t list
end
