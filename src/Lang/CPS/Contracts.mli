
(** Contract [right_scopes] states that every variable is bound in the program.
*)
val right_scopes : Contract.t

(** Contract [unique_vars] states that there are no two different local
variables represeted by the same value (of type [Common.var]). *)
val unique_vars  : Contract.t

(** Contract [unique_tags] states that every tag in the program is unique. *)
val unique_tags  : Contract.t

(** Contract [primop_arity] states that every primop operation has right number
of arguments, binds right number of variables and has right number of
continuations *)
val primop_arity : Contract.t

val closure_conversion : Contract.t

(* Contract [list_definitions] states that every function definition is in
 * top level fix *)
val lift_definitions : Contract.t
