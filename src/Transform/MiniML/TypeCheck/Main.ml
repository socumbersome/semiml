module S  = Lang.RawMiniML.Ast
module T  = Lang.MiniML.Ast
module TT = Lang.MiniML.Type
module TV = Lang.MiniML.TypeView
module TNF = Lang.MiniML.TypeNF
module Box = Printing.Box
module MDD = MetaData.MiniML.Datatypes

module Int64Set = Set.Make(Int64)

type tmp_fix_def =
  { tfxd_tag  : Common.Tag.t
  ; tfxd_var  : T.var
  ; tfxd_args : S.arg list
  ; tfxd_body : S.expr
  ; tfxd_type : TT.t
  }

let mk_expr tag kind =
  { T.e_tag  = tag
  ; T.e_kind = kind
  }

let infer_var_type env tag x =
  match Env.get_var env x with
  | Env.Var(x, tp) ->
    (mk_expr tag (T.Var x), tp)
  | Env.Primop(op, tp) ->
    (mk_expr tag (T.Prim op), tp)
  | Env.ConstCtor(ctor, 0, c) ->
    let info =
      { MDD.env      = Env.type_env env
      ; MDD.args     = []
      ; MDD.datatype = Env.get_datatype env c
      } in
    MDD.set tag info;
    (mk_expr tag (T.Con(ctor, [], None)), TT.TyCon([], c))
  | Env.ConstCtor(_, n, _) ->
    Errors.error ~tag: tag "Constructor %s expects %d type parameter(s)." x n;
    raise Errors.Fatal_error
  | Env.DataCtor(_, [], _, _) ->
    Errors.error ~tag: tag "Constructor %s expects an argument." x;
    raise Errors.Fatal_error
  | Env.DataCtor(_, args, _, _) ->
    Errors.error ~tag: tag
      "Constructor %s expects %d type parameter(s) and one argument."
      x
      (List.length args);
    raise Errors.Fatal_error
  | exception Not_found ->
    Errors.error ~tag: tag
      "Unbound variable or constructor %s." x;
    raise Errors.Fatal_error

type expr_of_ctor =
| EC_Expr        of T.expr * TT.t
| EC_DataCtor    of TT.cname * TT.t list * TT.tcon * TT.t
| EC_ConstCtorFn of TT.cname * TT.t list * int * TT.tcon
| EC_DataCtorFn  of TT.cname * TT.t list * TT.tvar list * TT.tcon * TV.t

let rec infer_expr_or_ctor_type env expr =
  let tag = expr.S.e_tag in
  match expr.S.e_kind with
  | S.Var x ->
    begin match Env.get_var env x with
    | Env.Var(x, tp) ->
      EC_Expr(mk_expr tag (T.Var x), tp)
    | Env.Primop(op, tp) ->
      EC_Expr(mk_expr tag (T.Prim op), tp)
    | Env.ConstCtor(ctor, 0, c) ->
      let info =
        { MDD.env      = Env.type_env env
        ; MDD.args     = []
        ; MDD.datatype = Env.get_datatype env c
        } in
      MDD.set tag info;
      EC_Expr(mk_expr tag (T.Con(ctor, [], None)), TT.TyCon([], c))
    | Env.ConstCtor(ctor, n, c) -> EC_ConstCtorFn(ctor, [], n, c)
    | Env.DataCtor(ctor, [], c, arg_tp) ->
      EC_DataCtor(ctor, [], c, arg_tp)
    | Env.DataCtor(ctor, args, c, arg_tp) ->
      EC_DataCtorFn(ctor, [], args, c, TV.of_type arg_tp)
    | exception Not_found ->
      Errors.error ~tag: tag
        "Unbound variable or constructor %s." x;
      raise Errors.Fatal_error
    end
  | S.TypeVarApp(e1, tp) -> infer_expr_or_ctor_type_app tag env e1 tp
  | _ -> let (e, tp) = infer_type env expr in EC_Expr(e, tp)

and infer_app_type tag env e1 e2 =
  match infer_expr_or_ctor_type env e1 with
  | EC_Expr(e1, tp1) ->
    begin match TNF.nf (Env.type_env env) tp1 with
    | TNF.Arrow(arg_tp, val_tp) ->
      let e2 = check_type env e2 (TV.to_type arg_tp) in
      (mk_expr tag (T.App(e1, e2)), TV.to_type val_tp)
    | TNF.Abstract([arg_tp], c) when TT.TCon.equal c TT.t_exn_ctor ->
      let e2 = check_type env e2 (TV.to_type arg_tp) in
      (mk_expr tag (T.ConExn(e1, e2)), TT.TyCon([], TT.t_exn))
    | _ ->
      let scope = Env.type_scope env in
      Errors.error_b ~tag: e1.T.e_tag
        [ Box.text "This expression has type"
        ; Box.indent 2 (Box.white_sep 
            (Lang.MiniML.Pretty.pretty_type scope 0 tp1)) |> Box.br
        ; Box.text "This is not a function; it cannot be applied."
        ];
      raise Errors.Fatal_error
    end
  | EC_DataCtor(ctor, tp_args, c, arg_tp) ->
    let e2 = check_type env e2 arg_tp in
    let info =
      { MDD.env      = Env.type_env env
      ; MDD.args     = tp_args
      ; MDD.datatype = Env.get_datatype env c
      } in
    MDD.set tag info;
    (mk_expr tag (T.Con(ctor, tp_args, Some e2)), TT.TyCon(tp_args, c))
  | EC_ConstCtorFn(_, args, n, _) ->
    Errors.error ~tag: e1.S.e_tag
      "This constructor expects %d type parameter(s), \
      but is applied here to %d parameter(s)."
      (n + List.length args)
      (List.length args);
    raise Errors.Fatal_error
  | EC_DataCtorFn(_, args, fargs, _, _) ->
    Errors.error ~tag: e1.S.e_tag
      "This constructor expects %d type parameter(s), \
      but is applied here to %d parameter(s)."
      (List.length args + List.length fargs)
      (List.length args);
    raise Errors.Fatal_error

and infer_expr_or_ctor_type_app tag env e1 tp =
  match infer_expr_or_ctor_type env e1 with
  | EC_Expr(e1, tp1) ->
    begin match Env.try_type_as_forall_var env tp1 with
    | Some bind ->
      let tp = Type.check env tp in
      let (x, body) = TV.open_tvar bind in
      let rtp = TV.to_type (TV.subst x (TV.of_type tp) body) in
      EC_Expr(mk_expr tag (T.TypeVarApp(e1, tp)), rtp)
    | None ->
      let scope = Env.type_scope env in
      Errors.error_b ~tag: e1.T.e_tag
        [ Box.text "This expression has type"
        ; Box.indent 2 (Box.white_sep
            (Lang.MiniML.Pretty.pretty_type scope 0 tp1)) |> Box.br
        ; Box.text "This is not a type abstraction; it cannot be applied."
        ];
      raise Errors.Fatal_error
    end
  | EC_DataCtor _ ->
    Errors.error ~tag: e1.S.e_tag
      "This constructor expects one argument.";
    raise Errors.Fatal_error
  | EC_ConstCtorFn(ctor, args, n, c) ->
    let tp = Type.check env tp in
    let args = tp :: args in
    let n = n - 1 in
    if n = 0 then begin
      let args = List.rev args in
      let info =
        { MDD.env      = Env.type_env env
        ; MDD.args     = args
        ; MDD.datatype = Env.get_datatype env c
        } in
      MDD.set tag info;
      EC_Expr(mk_expr tag (T.Con(ctor, args, None)), TT.TyCon(args, c))
    end else
      EC_ConstCtorFn(ctor, args, n, c)
  | EC_DataCtorFn(ctor, args, fargs, c, arg_tp) ->
    let tp = Type.check env tp in
    let args = tp :: args in
    begin match fargs with
    | [] -> assert false
    | [ x ] ->
      let args = List.rev args in
      let arg_tp = TV.to_type (TV.subst x (TV.of_type tp) arg_tp) in
      EC_DataCtor(ctor, args, c, arg_tp)
    | x :: fargs ->
      let arg_tp = TV.subst x (TV.of_type tp) arg_tp in
      EC_DataCtorFn(ctor, args, fargs, c, arg_tp)
    end

and infer_type_var_app_type tag env e1 tp =
  match infer_expr_or_ctor_type_app tag env e1 tp with
  | EC_Expr(e, tp) -> (e, tp)
  | EC_DataCtor _ ->
    Errors.error ~tag: e1.S.e_tag
      "This constructor expects one argument.";
    raise Errors.Fatal_error
  | EC_ConstCtorFn(_, args, n, _) ->
    Errors.error ~tag: e1.S.e_tag
      "This constructor expects %d type parameter(s), \
      but is applied here to %d parameter(s)."
      (n + List.length args)
      (List.length args);
    raise Errors.Fatal_error
  | EC_DataCtorFn(_, args, fargs, _, _) ->
    Errors.error ~tag: e1.S.e_tag
      "This constructor expects %d type parameter(s), \
      but is applied here to %d parameter(s)."
      (List.length args + List.length fargs)
      (List.length args);
    raise Errors.Fatal_error

and infer_type env expr =
  let tag = expr.S.e_tag in
  match expr.S.e_kind with
  | S.Var x          -> infer_var_type env tag x
  | S.Fn(args, body) -> infer_function_type tag env args body
  | S.Let(x, e1, e2) ->
    let (e1, tp1) = infer_type env e1 in
    let (env, x)  = Env.add_var env x.S.id_name tp1 in
    let (e2, tp2) = infer_type env e2 in
    let res = mk_expr tag (T.App(mk_expr tag (T.Fn(x, tp1, e2)), e1)) in
    (res, tp2)
  | S.Fix(fxds, e2) ->
    let (env, tmp_fxds) = build_fix_env env fxds in
    let fxds = List.map (check_fix_def env) tmp_fxds in
    let (e2, tp2) = infer_type env e2 in
    let res = mk_expr tag (T.Fix(fxds, e2)) in
    (res, tp2)
  | S.App(e1, e2) -> infer_app_type tag env e1 e2
  | S.TypeVarApp(e1, tp) -> infer_type_var_app_type tag env e1 tp
  | S.TypeApp(e1, typedefs) ->
    let (e1, tp1) = infer_type env e1 in
    infer_type_app_type tag env e1 tp1 typedefs
  | S.Int n ->
    (mk_expr tag (T.Int n), TT.TyCon([], TT.t_int))
  | S.Real r ->
    (mk_expr tag (T.Real r), TT.TyCon([], TT.t_real))
  | S.String s ->
    (mk_expr tag (T.String s), TT.TyCon([], TT.t_string))
  | S.Case(_, [], _) -> assert false
  | S.Case(e1, case0 :: cases, defc) -> 
    infer_case_expr_type tag env e1 case0 cases defc
  | S.CaseInt(_, [], _) -> assert false
  | S.CaseInt(e1, (n0, body0) :: cases, defc) ->
    let int_tp = TT.TyCon([], TT.t_int) in
    let e1 = check_type env e1 int_tp in
    let (body0, tp) = infer_type env body0 in
    let cases = check_int_cases env cases (Int64Set.singleton n0) tp in
    let defc = check_default_case env defc int_tp tp in
    (mk_expr tag (T.CaseInt(e1, (n0, body0) :: cases, defc)), tp)
  | S.CaseExn(_, [], _) -> assert false
  | S.CaseExn(e1, case0 :: cases, defc) ->
    let exn_tp = TT.TyCon([], TT.t_exn) in
    let e1 = check_type env e1 exn_tp in
    let (case0, tp) = infer_exn_case_type env case0 in
    let cases = check_exn_cases env cases tp in
    let defc = check_default_case env defc exn_tp tp in
    (mk_expr tag (T.CaseExn(e1, case0 :: cases, defc)), tp)
  | S.Exn e ->
    let e = check_type env e (TT.TyCon([], TT.t_string)) in
    (mk_expr tag (T.Exn e), TT.TyCon([], TT.t_exn))
  | S.ExnCtor(e, tp) ->
    let e  = check_type env e (TT.TyCon([], TT.t_string)) in
    let tp = Type.check env tp in
    (mk_expr tag (T.ExnCtor(e, tp)), TT.TyCon([tp], TT.t_exn_ctor))
  | S.ExnName e ->
    let e = check_type env e (TT.TyCon([], TT.t_exn)) in
    (mk_expr tag (T.ExnName e), TT.TyCon([], TT.t_string))
  | S.Record es ->
    let (es, tps) = infer_record_type env es in
    (mk_expr tag (T.Record es), TT.Record tps)
  | S.Select(i, e) ->
    let (e, tp) = infer_type env e in
    begin match Env.try_type_as_record env tp with
    | Some tps ->
      if i >= 0 && i < List.length tps then
        (mk_expr tag (T.Select(i, e)), List.nth tps i)
      else begin
        let scope = Env.type_scope env in
        Errors.error_b ~tag: e.T.e_tag
        [ Box.text "This expression has type"
        ; Box.indent 2 (Box.white_sep
            (Lang.MiniML.Pretty.pretty_type scope 0 tp)) |> Box.br
        ; Box.text (Printf.sprintf 
            "This is a record with %d field(s); it has no field %d."
            (List.length tps)
            i)
        ];
        raise Errors.Fatal_error
      end
    | None ->
      let scope = Env.type_scope env in
      Errors.error_b ~tag: e.T.e_tag
        [ Box.text "This expression has type"
        ; Box.indent 2 (Box.white_sep
            (Lang.MiniML.Pretty.pretty_type scope 0 tp)) |> Box.br
        ; Box.text (Printf.sprintf 
            "This is not a record; it has not field %d."
            i)
        ];
      raise Errors.Fatal_error
    end
  | S.Raise _ ->
    Errors.error ~tag: tag
      "Cannot infer type of this expression";
    raise Errors.Fatal_error
  | S.Handle(e1, e2) ->
    let (e1, tp) = infer_type env e1 in
    let e2 = check_type env e2 (TT.Arrow(TT.TyCon([], TT.t_exn), tp)) in
    (mk_expr tag (T.Handle(e1, e2)), tp)
  | S.TypeDef(defs, e) ->
    infer_local_typedefs tag env defs e
  | S.Datatype(defs, e) ->
    let (env, defs) = Type.check_datatypes env defs in
    let (e, tp) = infer_type env e in
    (mk_expr tag (T.DataType(defs, e)), TT.DataType(defs, tp))
  | S.Pack(_, _, None) ->
    Errors.error ~tag: tag
      "Cannot infer type of this expression";
    raise Errors.Fatal_error
  | S.Pack(tdefs, body, Some tp) ->
    let tp = build_pack_type env tdefs tp in
    (check_pack_type tag env tdefs body tp, tp)
  | S.Unpack _ ->
    Errors.error ~tag: tag
      "Cannot infer type of this expression";
    raise Errors.Fatal_error
  | S.Annot(e, tp) ->
    let tp = Type.check env tp in
    (check_type env e tp, tp)

and infer_function_type tag env args body =
  let (env, mke, mkt) = Arg.infer_types tag env args in
  let (body, tp) = infer_type env body in
  (mke body, mkt tp)

and infer_type_app_type tag env e1 tp1 typedefs =
  match typedefs with
  | [] -> (e1, tp1)
  | typedef :: typedefs ->
    begin match Env.try_type_as_forall env tp1 with
    | Some bind ->
      let (args, body) = Type.check_typedef env typedef in
      let (n, x, tp) = TV.open_tcon bind in
      if n = List.length args then begin
        let e1  = mk_expr tag (T.TypeApp(e1, args, body)) in
        let tp1 = TT.TypeDef(args, x, body, TV.to_type tp) in
        infer_type_app_type tag env e1 tp1 typedefs
      end else begin
        let scope = Env.type_scope env in
        Errors.error_b ~tag: typedef.S.tdef_tag
          [ Box.text "Expression applied to this type has type"
          ; Box.indent 2 (Box.white_sep
              (Lang.MiniML.Pretty.pretty_type scope 0 tp1)) |> Box.br
          ; Box.text (Printf.sprintf
              "It expects type with %d parameter(s), \
              but it is applied to type with %d parameter(s)."
              n
              (List.length args))
          ];
        raise Errors.Fatal_error
      end
    | None ->
      let scope = Env.type_scope env in
      Errors.error_b ~tag: typedef.S.tdef_tag
        [ Box.text "Expression applied to this type has type"
        ; Box.indent 2 (Box.white_sep
            (Lang.MiniML.Pretty.pretty_type scope 0 tp1)) |> Box.br
        ; Box.text "This is not a type function; it cannot be applied."
        ];
      raise Errors.Fatal_error
    end

and infer_case_expr_type tag env e1 case0 cases defc =
  match Env.get_var env case0.S.cc_ctor.S.id_name with
  | Env.Var _ | Env.Primop _ ->
    Errors.error ~tag: case0.S.cc_ctor.S.id_tag
      "%s is not a constructor." case0.S.cc_ctor.S.id_name;
    raise Errors.Fatal_error
  | Env.ConstCtor(_, _, c) | Env.DataCtor(_, _, c, _) ->
    let (e1, tp1) = infer_type env e1 in
    begin match TNF.nf (Env.type_env env) tp1 with
    | TNF.DataType(args, _, _, _) | TNF.LocalDataType(_, args, _, _) ->
      let args = List.map TV.to_type args in
      if Env.type_equal env tp1 (TT.TyCon(args, c)) then begin
        let ctors = Env.ctors_of_datatype env c in
        let (case0, unchecked_ctors, tp) =
          infer_ctor_case_type
            ~env:       env
            ~tp_args:   args
            ~tcon:      c
            ~all_ctors: ctors
            case0 in
        let (cases, unchecked_ctors) = 
          check_ctor_cases 
            ~env:             env 
            ~tp_args:         args
            ~tcon:            c
            ~all_ctors:       ctors
            ~unchecked_ctors: unchecked_ctors
            cases
            tp
        in
        let defc = 
          check_ctor_default_case
            tag
            env
            unchecked_ctors
            (TT.TyCon(args, c)) 
            defc
            tp in
        let info =
          { MDD.env      = Env.type_env env
          ; MDD.args     = args
          ; MDD.datatype = Env.get_datatype env c
          } in
        MDD.set tag info;
        (mk_expr tag (T.Case
          { T.ce_expr         = e1
          ; T.ce_cases        = case0 :: cases
          ; T.ce_default_case = defc
          }), tp)
      end else begin
        let scope = Env.type_scope env in
        Errors.error_b ~tag: e1.T.e_tag
        [ Box.text "This expression has type"
        ; Box.indent 2 (Box.white_sep
            (Lang.MiniML.Pretty.pretty_type scope 0 tp1)) |> Box.br
        ; Box.text (Printf.sprintf
            "This type is incompatibile with datatype %s."
            (TT.TCon.name c))
        ];
        raise Errors.Fatal_error
      end
    | _ ->
      let scope = Env.type_scope env in
      Errors.error_b ~tag: e1.T.e_tag
      [ Box.text "This expression has type"
      ; Box.indent 2 (Box.white_sep
          (Lang.MiniML.Pretty.pretty_type scope 0 tp1)) |> Box.br
      ; Box.text "This is not a datatype."
      ];
      raise Errors.Fatal_error
    end
  | exception Not_found ->
    Errors.error ~tag: case0.S.cc_ctor.S.id_tag
      "Unbound constructor %s." case0.S.cc_ctor.S.id_name;
    raise Errors.Fatal_error

and infer_ctor_case_type ~env ~tp_args ~tcon ~all_ctors case =
  match Env.get_var env case.S.cc_ctor.S.id_name with
  | Env.Var _ | Env.Primop _ ->
    Errors.error ~tag: case.S.cc_ctor.S.id_tag
      "%s is not a constructor." case.S.cc_ctor.S.id_name;
    raise Errors.Fatal_error
  | Env.ConstCtor(c, _, _) ->
    if not (TT.CName.Set.mem c all_ctors) then begin
      Errors.error ~tag: case.S.cc_ctor.S.id_tag
        "The constructor %s does not belong to type %s."
        case.S.cc_ctor.S.id_name
        (TT.TCon.name tcon);
      raise Errors.Fatal_error
    end;
    begin match case.S.cc_arg with
    | None ->
      let (body, tp) = infer_type env case.S.cc_body in
      ((c, None, body), TT.CName.Set.remove c all_ctors, tp)
    | Some x ->
      Errors.error ~tag: x.S.id_tag
        "The constructor %s expects no argument"
        case.S.cc_ctor.S.id_name;
      raise Errors.Fatal_error
    end
  | Env.DataCtor(c, fargs, _, arg_type) ->
    if not (TT.CName.Set.mem c all_ctors) then begin
      Errors.error ~tag: case.S.cc_ctor.S.id_tag
        "The constructor %s does not belong to type %s."
        case.S.cc_ctor.S.id_name
        (TT.TCon.name tcon);
      raise Errors.Fatal_error
    end;
    begin match case.S.cc_arg with
    | None ->
      Errors.error ~tag: case.S.cc_ctor.S.id_tag
        "The constructor %s expects an argument"
        case.S.cc_ctor.S.id_name;
      raise Errors.Fatal_error
    | Some x ->
      let arg_type = TV.to_type (List.fold_left2 
        (fun tp x a ->
          TV.subst x (TV.of_type a) tp
        ) (TV.of_type arg_type) fargs tp_args) in
      let (body_env, x) = Env.add_var env x.S.id_name arg_type in
      let (body, tp) = infer_type body_env case.S.cc_body in
      ((c, Some x, body), TT.CName.Set.remove c all_ctors, tp)
    end
  | exception Not_found ->
    Errors.error ~tag: case.S.cc_ctor.S.id_tag
      "Unbound constructor %s." case.S.cc_ctor.S.id_name;
    raise Errors.Fatal_error

and infer_exn_case_type env case =
  match case.S.ec_arg with
  | None ->
    let ee = check_type env case.S.ec_exn (TT.TyCon([], TT.t_exn)) in
    let (body, tp) = infer_type env case.S.ec_body in
    ((ee, None, body), tp)
  | Some x ->
    let (ee, exn_tp) = infer_type env case.S.ec_exn in
    begin match TNF.nf (Env.type_env env) exn_tp with
    | TNF.Abstract([arg_tp], c) when TT.TCon.equal c TT.t_exn_ctor ->
      let (body_env, x) = Env.add_var env x.S.id_name (TV.to_type arg_tp) in
      let (body, tp) = infer_type body_env case.S.ec_body in
      ((ee, Some x, body), tp)
    | _ ->
      let scope = Env.type_scope env in
      Errors.error_b ~tag: ee.T.e_tag
        [ Box.text "This expression has type"
        ; Box.indent 2 (Box.white_sep 
            (Lang.MiniML.Pretty.pretty_type scope 0 exn_tp)) |> Box.br
        ; Box.text "This is not an exception econstructor."
        ];
      raise Errors.Fatal_error
    end

and infer_record_type env es =
  match es with
  | [] -> ([], [])
  | e :: es ->
    let (e,  tp)  = infer_type env e in
    let (es, tps) = infer_record_type env es in
    (e :: es, tp :: tps)

and infer_local_typedefs tag env defs expr =
  match defs with
  | [] -> infer_type env expr
  | def :: defs ->
    let (args, body) = Type.check_typedef env def in
    let (env, x) = Env.add_typedef env def.S.tdef_name.S.id_name args body in
    let (expr, tp) = infer_local_typedefs tag env defs expr in
    ( mk_expr tag (T.TypeDef(args, x, body, expr))
    , TT.TypeDef(args, x, body, tp))

and build_pack_type env tdefs tp =
  match tdefs with
  | [] -> Type.check env tp
  | tdef :: tdefs ->
    let n = List.length tdef.S.tdef_args in
    let (env, c) = Env.add_abstype env tdef.S.tdef_name.S.id_name n in
    let tp = build_pack_type env tdefs tp in
    TT.Exists(n, c, tp)

and build_fix_env env fxds =
  match fxds with
  | [] -> (env, [])
  | fxd :: fxds ->
    let tag = fxd.S.fxd_tag in
    let (body_env, _, mk_tp) = Arg.infer_types tag env fxd.S.fxd_args in
    let body_type = Type.check body_env fxd.S.fxd_type in
    let tp = mk_tp body_type in
    let (env, x) = Env.add_var env fxd.S.fxd_name.S.id_name tp in
    let (env, fxds) = build_fix_env env fxds in
    (env,
      { tfxd_tag  = tag
      ; tfxd_var  = x
      ; tfxd_args = fxd.S.fxd_args
      ; tfxd_body = fxd.S.fxd_body
      ; tfxd_type = tp
      } :: fxds)

and check_fix_def env tmp_fxd =
  let body = 
    check_function_type
      tmp_fxd.tfxd_tag
      env
      tmp_fxd.tfxd_args
      tmp_fxd.tfxd_body
      tmp_fxd.tfxd_type
  in
  if not (T.Expr.is_function body) then begin
    Errors.error ~tag: tmp_fxd.tfxd_tag
      "Recursive values are prohibited.";
    raise Errors.Fatal_error
  end;
  { T.fxd_var  = tmp_fxd.tfxd_var
  ; T.fxd_body = body
  ; T.fxd_type = tmp_fxd.tfxd_type
  }

and check_type env expr tp =
  let tag = expr.S.e_tag in
  match expr.S.e_kind with
  | S.Var _ | S.App _ | S.TypeVarApp _ | S.TypeApp _ | S.Int _ | S.Real _ 
  | S.String _ | S.Exn _ | S.ExnCtor _ | S.ExnName _ | S.Select _ 
  | S.Pack(_, _, Some _) | S.Annot _ ->
    let (expr, rtp) = infer_type env expr in
    if Env.type_equal env tp rtp then expr
    else begin
      let scope = Env.type_scope env in
      Errors.error_b ~tag: tag
        [ Box.text "This expression has type"
        ; Box.indent 2 (Box.white_sep 
            (Lang.MiniML.Pretty.pretty_type scope 0 rtp))
        ; Box.white_sep (Box.text "but an expression was expected of type")
        ; Box.indent 2 (Box.white_sep 
            (Lang.MiniML.Pretty.pretty_type scope 0 tp))
        ];
      raise Errors.Fatal_error
    end
  | S.Fn(args, body) -> check_function_type tag env args body tp
  | S.Let(x, e1, e2) ->
    let (e1, tp1) = infer_type env e1 in
    let (env, x)  = Env.add_var env x.S.id_name tp1 in
    let e2 = check_type env e2 tp in
    mk_expr tag (T.App(mk_expr tag (T.Fn(x, tp1, e2)), e1))
  | S.Fix(fxds, e2) ->
    let (env, tmp_fxds) = build_fix_env env fxds in
    let fxds = List.map (check_fix_def env) tmp_fxds in
    let e2 = check_type env e2 tp in
    mk_expr tag (T.Fix(fxds, e2))
  | S.Case(_, [], _) -> assert false
  | S.Case(e1, case0 :: cases, defc) -> 
    check_case_expr tag env e1 case0 cases defc tp
  | S.CaseInt(e1, cases, defc) ->
    let int_tp = TT.TyCon([], TT.t_int) in
    let e1 = check_type env e1 int_tp in
    let cases = check_int_cases env cases Int64Set.empty tp in
    let defc = check_default_case env defc int_tp tp in
    mk_expr tag (T.CaseInt(e1, cases, defc))
  | S.CaseExn(e1, cases, defc) ->
    let exn_tp = TT.TyCon([], TT.t_exn) in
    let e1 = check_type env e1 exn_tp in
    let cases = check_exn_cases env cases tp in
    let defc = check_default_case env defc exn_tp tp in
    mk_expr tag (T.CaseExn(e1, cases, defc))
  | S.Record es ->
    begin match Env.try_type_as_record env tp with
    | None ->
      let scope = Env.type_scope env in
      Errors.error_b ~tag: tag
      [ Box.text
        "This expression is a record, but an expression was expected of type"
      ; Box.indent 2 (Box.white_sep
          (Lang.MiniML.Pretty.pretty_type scope 0 tp))
      ];
      raise Errors.Fatal_error
    | Some tps ->
      if List.length es <> List.length tps then begin
        Errors.error ~tag: tag
          "This record has %d field(s), \
          but a record was expected of %d field(s)"
          (List.length es)
          (List.length tps);
        raise Errors.Fatal_error
      end;
      mk_expr tag (T.Record (List.map2 (check_type env) es tps))
    end
  | S.Raise e ->
    let e = check_type env e (TT.TyCon([], TT.t_exn)) in
    mk_expr tag (T.Raise(e, tp))
  | S.Handle(e1, e2) ->
    let e1 = check_type env e1 tp in
    let e2 = check_type env e2 (TT.Arrow(TT.TyCon([], TT.t_exn), tp)) in
    mk_expr tag (T.Handle(e1, e2))
  | S.TypeDef(defs, e) ->
    check_local_typedefs tag env defs e tp
  | S.Datatype(defs, e) ->
    let (env, defs) = Type.check_datatypes env defs in
    let e = check_type env e tp in
    mk_expr tag (T.DataType(defs, e))
  | S.Pack(tdefs, body, None) ->
    check_pack_type tag env tdefs body tp
  | S.Unpack(decls, x, e1, e2) ->
    let (e1, tp1) = infer_type env e1 in
    check_unpack_type tag env decls x.S.id_name e1 tp1 e2 tp

and check_function_type tag env args body tp =
  let (env, mke, tp) = Arg.check_types tag env args tp in
  let body = check_type env body tp in
  mke body

and check_case_expr tag env e1 case0 cases defc tp =
  match Env.get_var env case0.S.cc_ctor.S.id_name with
  | Env.Var _ | Env.Primop _ ->
    Errors.error ~tag: case0.S.cc_ctor.S.id_tag
      "%s is not a constructor." case0.S.cc_ctor.S.id_name;
    raise Errors.Fatal_error
  | Env.ConstCtor(_, _, c) | Env.DataCtor(_, _, c, _) ->
    let (e1, tp1) = infer_type env e1 in
    begin match TNF.nf (Env.type_env env) tp1 with
    | TNF.DataType(args, _, _, _) | TNF.LocalDataType(_, args, _, _) ->
      let args = List.map TV.to_type args in
      if Env.type_equal env tp1 (TT.TyCon(args, c)) then begin
        let ctors = Env.ctors_of_datatype env c in
        let (cases, unchecked_ctors) = 
          check_ctor_cases 
            ~env:             env 
            ~tp_args:         args
            ~tcon:            c
            ~all_ctors:       ctors
            ~unchecked_ctors: ctors
            (case0 :: cases)
            tp
        in
        let defc = 
          check_ctor_default_case 
            tag 
            env 
            unchecked_ctors 
            (TT.TyCon(args, c)) 
            defc 
            tp in
        let info =
          { MDD.env      = Env.type_env env
          ; MDD.args     = args
          ; MDD.datatype = Env.get_datatype env c
          } in
        MDD.set tag info;
        mk_expr tag (T.Case
          { T.ce_expr         = e1
          ; T.ce_cases        = cases
          ; T.ce_default_case = defc
          })
      end else begin
        let scope = Env.type_scope env in
        Errors.error_b ~tag: e1.T.e_tag
        [ Box.text "This expression has type"
        ; Box.indent 2 (Box.white_sep
            (Lang.MiniML.Pretty.pretty_type scope 0 tp1)) |> Box.br
        ; Box.text (Printf.sprintf
            "This type is incompatibile with datatype %s."
            (TT.TCon.name c))
        ];
        raise Errors.Fatal_error
      end
    | _ ->
      let scope = Env.type_scope env in
      Errors.error_b ~tag: e1.T.e_tag
      [ Box.text "This expression has type"
      ; Box.indent 2 (Box.white_sep
          (Lang.MiniML.Pretty.pretty_type scope 0 tp1)) |> Box.br
      ; Box.text "This is not a datatype."
      ];
      raise Errors.Fatal_error
    end
  | exception Not_found ->
    Errors.error ~tag: case0.S.cc_ctor.S.id_tag
      "Unbound constructor %s." case0.S.cc_ctor.S.id_name;
    raise Errors.Fatal_error

and check_ctor_cases ~env ~tp_args ~tcon ~all_ctors ~unchecked_ctors cases tp =
  match cases with
  | [] -> ([], unchecked_ctors)
  | case :: cases ->
    begin match Env.get_var env case.S.cc_ctor.S.id_name with
    | Env.Var _ | Env.Primop _ ->
      Errors.error ~tag: case.S.cc_ctor.S.id_tag
        "%s is not a constructor." case.S.cc_ctor.S.id_name;
      raise Errors.Fatal_error
    | Env.ConstCtor(c, _, _) ->
      if not (TT.CName.Set.mem c all_ctors) then begin
        Errors.error ~tag: case.S.cc_ctor.S.id_tag
          "The constructor %s does not belong to type %s."
          case.S.cc_ctor.S.id_name
          (TT.TCon.name tcon);
        raise Errors.Fatal_error
      end;
      if not (TT.CName.Set.mem c unchecked_ctors) then begin
        Errors.warning ~tag: case.S.cc_ctor.S.id_tag
          "This case is redundant."
      end;
      begin match case.S.cc_arg with
      | None ->
        let body = check_type env case.S.cc_body tp in
        let (cases, unchecked_ctors) =
          check_ctor_cases
            ~env:             env
            ~tp_args:         tp_args
            ~tcon:            tcon
            ~all_ctors:       all_ctors
            ~unchecked_ctors: (TT.CName.Set.remove c unchecked_ctors)
            cases
            tp
        in ((c, None, body) :: cases, unchecked_ctors)
      | Some x ->
        Errors.error ~tag: x.S.id_tag
          "The constructor %s expects no argument"
          case.S.cc_ctor.S.id_name;
        raise Errors.Fatal_error
      end
    | Env.DataCtor(c, fargs, _, arg_type) ->
      if not (TT.CName.Set.mem c all_ctors) then begin
        Errors.error ~tag: case.S.cc_ctor.S.id_tag
          "The constructor %s does not belong to type %s."
          case.S.cc_ctor.S.id_name
          (TT.TCon.name tcon);
        raise Errors.Fatal_error
      end;
      if not (TT.CName.Set.mem c unchecked_ctors) then begin
        Errors.warning ~tag: case.S.cc_ctor.S.id_tag
          "This case is redundant."
      end;
      begin match case.S.cc_arg with
      | None ->
        Errors.error ~tag: case.S.cc_ctor.S.id_tag
          "The constructor %s expects an argument"
          case.S.cc_ctor.S.id_name;
        raise Errors.Fatal_error
      | Some x ->
        let arg_type = TV.to_type (List.fold_left2 
          (fun tp x a ->
            TV.subst x (TV.of_type a) tp
          ) (TV.of_type arg_type) fargs tp_args) in
        let (body_env, x) = Env.add_var env x.S.id_name arg_type in
        let body = check_type body_env case.S.cc_body tp in
        let (cases, unchecked_ctors) =
          check_ctor_cases
            ~env:             env
            ~tp_args:         tp_args
            ~tcon:            tcon
            ~all_ctors:       all_ctors
            ~unchecked_ctors: (TT.CName.Set.remove c unchecked_ctors)
            cases
            tp
        in ((c, Some x, body) :: cases, unchecked_ctors)
      end
    | exception Not_found ->
      Errors.error ~tag: case.S.cc_ctor.S.id_tag
        "Unbound constructor %s." case.S.cc_ctor.S.id_name;
      raise Errors.Fatal_error
    end

and check_ctor_default_case tag env unchecked_ctors tp_in defc tp_out =
  match defc with
  | None ->
    if TT.CName.Set.is_empty unchecked_ctors then None
    else begin
      Errors.error ~tag: tag
        "This pattern-matching is not exhaustive. \
        Constructor %s is not matched."
        (TT.CName.name (TT.CName.Set.choose unchecked_ctors));
      raise Errors.Fatal_error
    end
  | Some(x, body) ->
    if TT.CName.Set.is_empty unchecked_ctors then begin
      Errors.warning ~tag: body.S.e_tag
        "This case will be not used."
    end;
    let (env, x) = Env.add_var env x.S.id_name tp_in in
    let body = check_type env body tp_out in
    Some(x, body)

and check_int_cases env cases checked_values tp =
  match cases with
  | [] -> []
  | (n, body) :: cases ->
    if Int64Set.mem n checked_values then begin
      Errors.warning ~tag: body.S.e_tag
        "This case is redundant."
    end;
    let body = check_type env body tp in
    let cases = check_int_cases env cases (Int64Set.add n checked_values) tp in
    ((n, body) :: cases)

and check_exn_cases env cases tp =
  match cases with
  | [] -> []
  | { S.ec_arg = None } as case :: cases ->
    let ee = check_type env case.S.ec_exn (TT.TyCon([], TT.t_exn)) in
    let body = check_type env case.S.ec_body tp in
    let cases = check_exn_cases env cases tp in
    (ee, None, body) :: cases
  | { S.ec_arg = Some x } as case :: cases ->
    let (ee, exn_tp) = infer_type env case.S.ec_exn in
    begin match TNF.nf (Env.type_env env) exn_tp with
    | TNF.Abstract([arg_tp], c) when TT.TCon.equal c TT.t_exn_ctor ->
      let (body_env, x) = Env.add_var env x.S.id_name (TV.to_type arg_tp) in
      let body = check_type body_env case.S.ec_body tp in
      let cases = check_exn_cases env cases tp in
      (ee, Some x, body) :: cases
    | _ ->
      let scope = Env.type_scope env in
      Errors.error_b ~tag: ee.T.e_tag
        [ Box.text "This expression has type"
        ; Box.indent 2 (Box.white_sep 
            (Lang.MiniML.Pretty.pretty_type scope 0 exn_tp)) |> Box.br
        ; Box.text "This is not an exception econstructor."
        ];
      raise Errors.Fatal_error
    end

and check_default_case env (x, body) in_tp out_tp =
  let (env, x) = Env.add_var env x.S.id_name in_tp in
  let body = check_type env body out_tp in
  (x, body)

and check_local_typedefs tag env defs expr tp =
  match defs with
  | [] -> check_type env expr tp
  | def :: defs ->
    let (args, body) = Type.check_typedef env def in
    let (env, x) = Env.add_typedef env def.S.tdef_name.S.id_name args body in
    mk_expr tag 
      (T.TypeDef(args, x, body, check_local_typedefs tag env defs expr tp))

and check_pack_type tag env tdefs expr tp =
  match tdefs with
  | [] -> check_type env expr tp
  | tdef :: tdefs ->
    let (args, body) = Type.check_typedef env tdef in
    begin match Env.try_type_as_exists env tp with
    | None ->
      let scope = Env.type_scope env in
      Errors.error_b ~tag: tdef.S.tdef_tag
        [ Box.text "Packing a type in the context of type"
        ; Box.indent 2 (Box.white_sep
            (Lang.MiniML.Pretty.pretty_type scope 0 tp))
        ];
      raise Errors.Fatal_error
    | Some bind ->
      let n = List.length args in
      let (env, x) = 
        Env.add_typedef env tdef.S.tdef_name.S.id_name args body in
      let (m, new_tp)  = TV.open_fresh_tcon x bind in
      let new_tp = TV.to_type new_tp in
      if n <> m then begin
        let scope = Env.type_scope env in
        Errors.error_b ~tag: tdef.S.tdef_tag
        [ Box.text (Printf.sprintf
            "Packing a type with %d parameter(s) in the context of type"
            n)
        ; Box.indent 2 (Box.white_sep
            (Lang.MiniML.Pretty.pretty_type scope 0 tp)) |> Box.br
        ; Box.text (Printf.sprintf
            "Packed type should have %d parameter(s)."
            m)
        ];
        raise Errors.Fatal_error
      end;
      let expr = check_pack_type tag env tdefs expr new_tp in
      mk_expr tag (T.Pack(args, x, body, expr, new_tp))
    end

and check_unpack_type tag env decls x e1 tp1 e2 tp =
  match decls with
  | [] -> check_type env e2 tp
  | decl :: decls ->
    begin match Env.try_type_as_exists env tp1 with
    | None ->
      let scope = Env.type_scope env in
      Errors.error_b ~tag: decl.S.tdecl_tag
        [ Box.text "Unpacking a type from an expression of type"
        ; Box.indent 2 (Box.white_sep
            (Lang.MiniML.Pretty.pretty_type scope 0 tp1))
        ];
      raise Errors.Fatal_error
    | Some bind ->
      let n = List.length decl.S.tdecl_args in
      let (env, c) = Env.add_abstype env decl.S.tdecl_name.S.id_name n in
      let (m, new_tp1)  = TV.open_fresh_tcon c bind in
      let new_tp1 = TV.to_type new_tp1 in
      if n <> m then begin
        let scope = Env.type_scope env in
        Errors.error_b ~tag: decl.S.tdecl_tag
        [ Box.text (Printf.sprintf
            "Unpacking a type with %d parameter(s) from an expression of type"
            n)
        ; Box.indent 2 (Box.white_sep
            (Lang.MiniML.Pretty.pretty_type scope 0 tp1)) |> Box.br
        ; Box.text (Printf.sprintf
            "Packed type should have %d parameter(s)."
            m)
        ];
        raise Errors.Fatal_error
      end;
      let (env, y) = Env.add_var env x new_tp1 in
      let new_e1 = mk_expr tag (T.Var y) in
      mk_expr tag (T.Unpack(n, c, y, e1,
        check_unpack_type tag env decls x new_e1 new_tp1 e2 tp))
    end

let check expr =
  let (expr, _) = infer_type Env.empty expr in
  expr

let c_type_check = Contract.create
  ~description: "Type checking"
  ~languages: [Language.MiniML]
  "transform:type_check"

let register () =
  Compiler.register_transformation
    ~source: Compiler.Lang_RawMiniML
    ~target: Compiler.Lang_MiniML
    ~name:   "MiniML:type_check"
    ~contracts:
      [ Lang.MiniML.Contracts.well_typed
      ; c_type_check
      ]
    check
